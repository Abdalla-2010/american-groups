<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Alert Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain alert messages for various scenarios
    | during CRUD operations. You are free to modify these language lines
    | according to your application's requirements.
    |
    */

    'backend' => [
        'roles' => [
            'created' => '.لقد تم إضافة الدور الجديد بنجاح',
            'deleted' => 'لقد تم مسح الدور بنجاح.',
            'updated' => 'تم تعديل الدور بنجاح.',
        ],

        'users' => [
            'cant_resend_confirmation' => 'The application is currently set to manually approve users.',
            'confirmation_email' => 'لقد تم إرسال رسالة تأكيد جديدة إلى عنوان البريد الألكتروني الموجود في الملف الشخصي.',
            'confirmed' => 'The user was successfully confirmed.',
            'created' => 'لقد تم إنشاء المستخدم الجديد بنجاح.',
            'deleted' => 'لقد تم إزالة المستخدم بنجاح.',
            'deleted_permanently' => 'لقد تم حذف المستخدم نهائيا بنجاح.',
            'restored' => 'لقد تمت استعادة المستخدم بنجاح.',
            'session_cleared' => "The user's session was successfully cleared.",
            'social_deleted' => 'Social Account Successfully Removed',
            'unconfirmed' => 'The user was successfully un-confirmed',
            'updated' => 'لقد تم تعديل المستخدم بنجاح.',
            'updated_password' => 'لقد تم تعديل كلمة مرور المستخدم بنجاح.',
        ],

        'secretaries' => [
            'created' => '.لقد تم إضافة السكرتير الجديد بنجاح',
            'deleted' => 'لقد تم مسح السكرتير بنجاح.',
            'updated' => 'تم تعديل السكرتير بنجاح.',
        ],

        'branches' => [
            'created' => '.لقد تم إضافة الفرع الجديد بنجاح',
            'deleted' => 'لقد تم مسح الفرع بنجاح.',
            'updated' => 'تم تعديل الفرع بنجاح.',
        ],

        'technicals' => [
            'created' => '.لقد تم إضافة الفنى الجديد بنجاح',
            'deleted' => 'لقد تم مسح الفنى بنجاح.',
            'updated' => 'تم تعديل الفنى بنجاح.',
        ],

        'carriers' => [
            'created' => '.لقد تم إضافة السحّاب الجديد بنجاح',
            'deleted' => 'لقد تم مسح السحّاب بنجاح.',
            'updated' => 'تم تعديل السحّاب بنجاح.',
        ],

        'employees' => [
            'created' => '.لقد تم إضافة الموظف الجديد بنجاح',
            'deleted' => 'لقد تم مسح الموظف بنجاح.',
            'updated' => 'تم تعديل الموظف بنجاح.',
        ],

        'cities' => [
            'created' => '.لقد تم إضافة المدينة الجديدة بنجاح',
            'deleted' => 'لقد تم مسح المدينة بنجاح.',
            'updated' => 'تم تعديل المدينة بنجاح.',
        ],

        'orders' => [
            'created' => '.لقد تم إضافة الطلب الجديد بنجاح',
            'deleted' => 'لقد تم مسح الطلب بنجاح.',
            'updated' => 'تم تعديل الطلب بنجاح.',
        ],


    ],

    'frontend' => [
        'contact' => [
            'sent' => 'Your information was successfully sent. We will respond back to the e-mail provided as soon as we can.',
        ],
    ],
];
