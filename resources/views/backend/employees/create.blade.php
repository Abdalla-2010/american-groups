@extends('backend.layouts.app')

@section('title', __('labels.backend.employees.management') . ' | ' . __('labels.backend.employees.create'))


@section('content')
    {{ html()->form('POST', route('admin.employees.store'))->class('form-horizontal')->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        @lang('labels.backend.employees.management')
                        <small class="text-muted">@lang('labels.backend.employees.create')</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr>

            <div class="row mt-4 mb-4">
                <div class="col">

                    @if ($logged_in_user->role = config('access.users.branch_role'))
                        <div class="form-group row">
                            <div class="col-md-10">
                                {{ html()->text('branch_id')
                                    ->class('form-control')
                                    ->placeholder(__('labels.backend.employees.branch_id'))
                                    ->attribute('hidden', true)
                                    ->value(auth()->id())
                                     }}
                            </div><!--col-->
                        </div><!--form-group-->
                    @endif

                    @if($logged_in_user->isAdmin())
                        <div class="form-group row">
                            {{ html()->label(__('labels.backend.employees.branch'))->class('col-md-2 form-control-label')->for('branch_id') }}

                            <div class="col-md-10">
                                {{ html()->select('branch_id',['' => 'إختر الفرع'] + $branch->pluck('first_name', 'id')->toArray())
                                        ->required()
                                        ->class('form-control') }}
                            </div><!--col-->
                        </div><!--form-group-->
                    @endif

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.employees.first_name'))->class('col-md-2 form-control-label')->for('first_name') }}

                        <div class="col-md-10">
                            {{ html()->text('first_name')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.employees.first_name'))
                                ->attribute('maxlength', 191)
                                ->autofocus() }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.employees.last_name'))->class('col-md-2 form-control-label')->for('last_name') }}

                        <div class="col-md-10">
                            {{ html()->text('last_name')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.employees.last_name'))
                                ->attribute('maxlength', 191)
                                ->autofocus()
                                ->value(old('name'))
                                 }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.employees.phone'))->class('col-md-2 form-control-label')->for('phone') }}

                        <div class="col-md-10">
                            {{ html()->text('phone')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.employees.phone'))
                                ->attribute('maxlength', 191)
                                ->required()
                                ->value(old('phone'))
                                 }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.employees.second_phone'))->class('col-md-2 form-control-label')->for('second_phone') }}

                        <div class="col-md-10">
                            {{ html()->text('second_phone')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.employees.second_phone'))
                                ->attribute('maxlength', 191)
                                ->value(old('second_phone'))
                                 }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.employees.salary'))->class('col-md-2 form-control-label')->for('salary') }}

                        <div class="col-md-10">
                            {{ html()->number('salary')
                                    ->class('form-control')
                                    ->placeholder(__('labels.backend.employees.salary'))
                                    ->attribute('min',1)
                                    ->attribute('step','any')
                                    ->required()
                                    ->value(old('salary'))
                                 }}
                        </div><!--col-->
                    </div><!--form-group-->

                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer clearfix">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.employees.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.create')) }}
                </div><!--col-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
    {{ html()->form()->close() }}
@endsection
