@extends('backend.layouts.app')

@section('title', __('labels.backend.orders.management') . ' | ' . __('labels.backend.orders.carrier-assignment'))


@section('content')
    {{ html()->form('POST', $data->url->update)->class('form-horizontal')->open() }}

    @method('PUT')

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        @lang('labels.backend.orders.management')
                        <small class="text-muted">@lang('labels.backend.orders.carrier-assignment')</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr>

            <div class="row mt-4 mb-4">
                <div class="col">

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.orders.branch'))->class('col-md-2 form-control-label')->for('branch') }}

                        <div class="col-md-10">
                            {{ html()->select('branch_id',$branch->pluck('first_name','id'),$order_branch)
                                 ->class('form-control')
                                 ->id('branch')
                                 ->attribute('disabled')
                                 }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.orders.carrier'))->class('col-md-2 form-control-label')->for('carrier') }}

                        <div class="col-md-10">
                            <select id="carrier" name="carrier_id" class="form-control">
                                @foreach($carrier as $value)
                                    <option value="{{$value->id}}" {{$value->id == $order_carrier->toArray() ? 'selected' : ''}}
                                    >{{$value->first_name.' '.$value->last_name}}</option>
                                @endforeach
                            </select>
                        </div><!--col-->
                    </div><!--form-group-->


                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer clearfix">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.orders.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.edit')) }}
                </div><!--col-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
    {{ html()->form()->close() }}
@endsection

@push('after-scripts')

    <script type="text/javascript">

        $('#branch').on('change', function (e) {
            var ovals = {};
            ovals['id']=$(this).val();
            $Ajax('getCarriers',ovals);
            var branch_id = e.target.value;

        });

    </script>

@endpush
