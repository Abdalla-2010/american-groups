@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.orders.management'))

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        {{ __('labels.backend.orders.management') }}
                    </h4>
                </div><!--col-->

                <div class="col-sm-7">
                    <div class="btn-toolbar float-right" role="toolbar"
                         aria-label="@lang('labels.general.toolbar_btn_groups')">
                        <a href="{{ route('admin.orders.create') }}" class="btn btn-success ml-1" data-toggle="tooltip"
                           title="@lang('labels.general.create_new')"><i class="fas fa-plus-circle"></i></a>
                    </div><!--btn-toolbar-->
                </div><!--col-->
            </div><!--row-->

            <div class="row mt-4">
                <div class="col">
                    <div class="table-responsive">
                        <table class="table" id="result-table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>@lang('labels.backend.orders.customer_name')</th>
                                <th>@lang('labels.backend.orders.phone')</th>
                                <th>@lang('labels.backend.orders.city')</th>
                                <th>@lang('labels.backend.orders.device_type')</th>
                                <th>@lang('labels.backend.orders.status')</th>
                                <th>@lang('labels.backend.orders.insurance_start_date')</th>
                                <th>@lang('labels.backend.orders.insurance_end_date')</th>
                                <th>@lang('labels.backend.orders.assignment')</th>
                                <th>طباعة</th>
                                <th>@lang('labels.general.actions')</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->
    </div><!--card-->
@endsection

@push('after-scripts')
    <script>
        $dataTable('{{ route('admin.orders.index') }}', [
            {data: 'DT_RowIndex', name: 'id'},
            {data: 'customer_name', name: 'customer_name'},
            {data: 'phone', name: 'phone'},
            {data: 'city.name', name: 'name'},
            {data: 'device_type', name: 'device_type'},
            {
                data: 'status', name: 'status', render: function (data) {
                    if (data == 1)
                        return '<span class="btn btn-warning btn-sm"><p>منتظر</p></span>';
                    if (data == 2)
                        return '<span class="btn btn-info btn-sm"><p>تم تعيين فنى</p></span>';
                    if (data == 3)
                        return '<span class="btn btn-success btn-sm"><p>صيانة فورية</p></span>';
                    if (data == 4)
                        return '<span class="btn btn-success btn-sm"><p>كشف</p></span>';
                    if (data == 5)
                        return '<span class="btn btn-danger btn-sm"><p>يحتاج سحّاب</p></span>';
                    if (data == 6)
                        return '<span class="btn btn-info btn-sm"><p>تم تعيين سحّاب</p></span>';
                }
            },
            {
                data: 'insurance_start_date', name: 'insurance_start_date', render: function (data) {
                    if (data)
                        return data;
                    return 'غير محدد'
                }
            },
            {
                data: 'insurance_end_date', name: 'insurance_end_date', render: function (data) {
                    if (data)
                        return data;
                    return 'غير محدد'
                }
            },
            {

                data: 'id', name: 'id', render: function (data, type, row, meta) {
                    var url = document.URL;
                    if (row.status == 1)
                        return '<a class="" href= "' + url + '/' + data + '/technical-assignment">تعيين فنى</a><br>';
                    if (row.status == 5)
                        return '<a class="" href= "' + url + '/' + data + '/carrier-assignment">تعيين سحّاب</a><br>';
                    return 'لا يحتاج توزيع';

                }
            },
            {
                data: 'id', name: 'id', render: function (data, type, row, meta) {
                    var url = document.URL;
                    return '<a class="" href= "' + url + '/' + data + '/print">طباعة</a><br>';
                }
            },
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]);
    </script>
@endpush
