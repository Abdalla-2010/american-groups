@extends('backend.layouts.app')

@section('title', __('labels.backend.orders.management') . ' | ' . __('labels.backend.orders.create'))


@section('content')
    {{ html()->form('POST', route('admin.orders.store'))->class('form-horizontal')->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        @lang('labels.backend.orders.management')
                        <small class="text-muted">@lang('labels.backend.orders.create')</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr>

            <div class="row mt-4 mb-4">
                <div class="col">

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.orders.customer_name'))->class('col-md-2 form-control-label')->for('customer_name') }}

                        <div class="col-md-10">
                            {{ html()->text('customer_name')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.orders.customer_name'))
                                ->attribute('maxlength', 191)
                                ->autofocus()
                                ->value(old('customer_name'))
                                 }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.orders.phone'))->class('col-md-2 form-control-label')->for('phone') }}

                        <div class="col-md-10">
                            {{ html()->text('phone')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.orders.phone'))
                                ->attribute('maxlength', 191)
                                ->required()
                                ->value(old('phone'))
                                 }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.orders.second_phone'))->class('col-md-2 form-control-label')->for('second_phone') }}

                        <div class="col-md-10">
                            {{ html()->text('second_phone')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.orders.second_phone'))
                                ->attribute('maxlength', 191)
                                ->value(old('second_phone'))
                                 }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.orders.city'))->class('col-md-2 form-control-label')->for('city') }}

                        <div class="col-md-10">
                            <select class="form-control" name="city_id">
                                {{ html()->option('إختر المدينة')
                                        ->class('form-control')
                                        ->value('')
                                        ->attribute('selected',true)
                                        ->attribute('disabled',true)
                                         }}
                                @foreach($city as $value)
                                    {{ html()->option($value->name)
                                        ->class('form-control')
                                        ->value($value->id)
                                         }}
                                @endforeach
                            </select>
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.orders.description'))->class('col-md-2 form-control-label')->for('description') }}

                        <div class="col-md-10">
                            {{ html()->textarea('description')
                                    ->class('form-control')
                                    ->placeholder(__('labels.backend.orders.description'))
                                    ->attribute('rows',6)
                                    ->required('maxlength',500)
                                    ->value(old('description'))
                                 }}
                        </div><!--col-->
                    </div><!--form-group-->


                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.orders.device_type'))->class('col-md-2 form-control-label')->for('device_type') }}

                        <div class="col-md-10">
                            {{ html()->text('device_type')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.orders.device_type'))
                                ->attribute('maxlength', 191)
                                ->required()
                                ->value(old('device_type'))
                                 }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.orders.device_model'))->class('col-md-2 form-control-label')->for('device_model') }}

                        <div class="col-md-10">
                            {{ html()->text('device_model')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.orders.device_model'))
                                ->attribute('maxlength', 191)
                                ->value(old('device_model'))
                                 }}
                        </div><!--col-->
                    </div><!--form-group-->

                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer clearfix">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.orders.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.create')) }}
                </div><!--col-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
    {{ html()->form()->close() }}
@endsection
