<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-title">
                @lang('menus.backend.sidebar.general')
            </li>
            <li class="nav-item">
                <a class="nav-link {{
                    active_class(Route::is('admin/dashboard'))
                }}" href="{{ route('admin.dashboard') }}">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    @lang('menus.backend.sidebar.dashboard')
                </a>
            </li>

            @if (auth()->user()->role  == 'admin')
                <li class="nav-title">
                    @lang('menus.backend.sidebar.system')
                </li>

                <li class="nav-item nav-dropdown {{
                    active_class(Route::is('admin/auth*'), 'open')
                }}">
                    <a class="nav-link nav-dropdown-toggle {{
                        active_class(Route::is('admin/auth*'))
                    }}" href="#">
                        <i class="nav-icon far fa-user"></i>
                        @lang('menus.backend.access.title')

                        @if ($pending_approval > 0)
                            <span class="badge badge-danger">{{ $pending_approval }}</span>
                        @endif
                    </a>

                    <ul class="nav-dropdown-items">
{{--                        <li class="nav-item">--}}
{{--                            <a class="nav-link {{--}}
{{--                                active_class(Route::is('admin/auth/user*'))--}}
{{--                            }}" href="{{ route('admin.auth.user.index') }}">--}}
{{--                                @lang('labels.backend.access.users.management')--}}

{{--                                @if ($pending_approval > 0)--}}
{{--                                    <span class="badge badge-danger">{{ $pending_approval }}</span>--}}
{{--                                @endif--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item">--}}
{{--                            <a class="nav-link {{--}}
{{--                                active_class(Route::is('admin/auth/role*'))--}}
{{--                            }}" href="{{ route('admin.auth.role.index') }}">--}}
{{--                                @lang('labels.backend.access.roles.management')--}}
{{--                            </a>--}}
{{--                        </li>--}}
                        <li class="nav-item">
                            <a class="nav-link {{
                                active_class(Route::is('admin/auth/secretaries*'))
                            }}" href="{{ route('admin.auth.secretaries.index') }}">
                                @lang('labels.backend.secretaries.management')
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{
                                active_class(Route::is('admin/auth/branches*'))
                            }}" href="{{ route('admin.auth.branches.index') }}">
                                @lang('labels.backend.branches.management')
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{
                                active_class(Route::is('admin/auth/technicals*'))
                            }}" href="{{ route('admin.auth.technicals.index') }}">
                                @lang('labels.backend.technicals.management')
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{
                                active_class(Route::is('admin/auth/carriers*'))
                            }}" href="{{ route('admin.auth.carriers.index') }}">
                                @lang('labels.backend.carriers.management')
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="divider"></li>

                <li class="nav-item nav-dropdown {{
                    active_class(Route::is('admin/log-viewer*'), 'open')
                }}">
                        <a class="nav-link nav-dropdown-toggle {{
                            active_class(Route::is('admin/log-viewer*'))
                        }}" href="#">
                        <i class="nav-icon fas fa-list"></i> @lang('menus.backend.log-viewer.main')
                    </a>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a class="nav-link {{
                            active_class(Route::is('admin/log-viewer'))
                        }}" href="{{ route('log-viewer::dashboard') }}">
                                @lang('menus.backend.log-viewer.dashboard')
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{
                            active_class(Route::is('admin/log-viewer/logs*'))
                        }}" href="{{ route('log-viewer::logs.list') }}">
                                @lang('menus.backend.log-viewer.logs')
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item">
                    <a class="nav-link {{
                    active_class(Route::is('admin/cities'))
                }}" href="{{ route('admin.cities.index') }}">
                        <i class="nav-icon fas fa-place-of-worship"></i>
                        @lang('labels.backend.cities.management')
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link {{
                    active_class(Route::is('admin/employees'))
                }}" href="{{ route('admin.employees.index') }}">
                        <i class="nav-icon fas fa-user-friends"></i>
                        @lang('labels.backend.employees.management')
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link {{
                    active_class(Route::is('admin/orders'))
                }}" href="{{ route('admin.orders.index') }}">
                        <i class="nav-icon fas fa-list"></i>
                        @lang('labels.backend.orders.management')
                    </a>
                </li>

            @endif

            @if (auth()->user()->role == 'secretary')

                <li class="nav-item">
                    <a class="nav-link {{
                        active_class(Route::is('admin/orders'))
                    }}" href="{{ route('admin.orders.index') }}">
                        <i class="nav-icon fas fa-list"></i>
                        @lang('labels.backend.orders.management')
                    </a>
                </li>
            @endif
{{--            {{dd(auth()->user()->role)}}--}}

            @if (auth()->user()->role == 'branch')
                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link {{--}}
                                {{--active_class(Route::is('admin/auth/secretaries*'))--}}
                            {{--}}" href="{{ route('admin.auth.secretaries.index') }}">--}}
                        {{--@lang('labels.backend.secretaries.management')--}}
                    {{--</a>--}}
                {{--</li>--}}
                <li class="nav-item">
                    <a class="nav-link {{
                                active_class(Route::is('admin/auth/technicals*'))
                            }}" href="{{ route('admin.auth.technicals.index') }}">
                        @lang('labels.backend.technicals.management')
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{
                                active_class(Route::is('admin/auth/carriers*'))
                            }}" href="{{ route('admin.auth.carriers.index') }}">
                        @lang('labels.backend.carriers.management')
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{
                        active_class(Route::is('admin/employees'))
                    }}" href="{{ route('admin.employees.index') }}">
                        <i class="nav-icon fas fa-list"></i>
                        @lang('labels.backend.employees.management')
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{
                        active_class(Route::is('admin/orders'))
                    }}" href="{{ route('admin.orders.index') }}">
                        <i class="nav-icon fas fa-list"></i>
                        @lang('labels.backend.orders.management')
                    </a>
                </li>

            @endif
        </ul>
    </nav>

    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div><!--sidebar-->
