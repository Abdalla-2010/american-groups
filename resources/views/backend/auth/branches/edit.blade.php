@extends('backend.layouts.app')

@section('title', __('labels.backend.branches.management') . ' | ' . __('labels.backend.branches.edit'))


@section('content')
    {{ html()->form('POST', $data->url->update)->class('form-horizontal')->acceptsFiles()->open() }}

    @method('PUT')

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        @lang('labels.backend.branches.management')
                        <small class="text-muted">@lang('labels.backend.branches.edit')</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr>

            <div class="row mt-4 mb-4">
                <div class="col">

                    <div class="form-group row">
                        <div class="col-md-10">
                            {{ html()->text('role')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.branches.role'))
                                ->attribute('hidden', true)
                                ->value('branch')
                                 }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.branches.first_name'))->class('col-md-2 form-control-label')->for('first_name') }}

                        <div class="col-md-10">
                            {{ html()->text('first_name')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.branches.first_name'))
                                ->attribute('maxlength', 191)
                                ->required()
                                ->value(old('first_name',$data->first_name))
                                 }}
                        </div><!--col-->
                    </div><!--form-group-->


                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.branches.email'))->class('col-md-2 form-control-label')->for('email') }}

                        <div class="col-md-10">
                            {{ html()->email('email')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.branches.email'))
                                ->attribute('maxlength', 191)
                                ->required()
                                ->value(old('email',$data->email))
                                 }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.branches.password'))->class('col-md-2 form-control-label')->for('password') }}

                        <div class="col-md-10">
                            {{ html()->password('password')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.branches.password'))
                                ->attribute('maxlength', 191)
                                 }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.branches.phone'))->class('col-md-2 form-control-label')->for('phone') }}

                        <div class="col-md-10">
                            {{ html()->text('phone')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.branches.phone'))
                                ->attribute('maxlength', 191)
                                ->required()
                                ->value(old('phone',$data->phone))
                                 }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.branches.second_phone'))->class('col-md-2 form-control-label')->for('second_phone') }}

                        <div class="col-md-10">
                            {{ html()->text('second_phone')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.branches.second_phone'))
                                ->attribute('maxlength', 191)
                                ->value(old('second_phone',$data->second_phone))
                                 }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.branches.address'))->class('col-md-2 form-control-label')->for('address') }}

                        <div class="col-md-10">
                            {{ html()->text('address')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.branches.address'))
                                ->attribute('maxlength', 500)
                                ->value(old('address',$data->address))
                                 }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.branches.id_number'))->class('col-md-2 form-control-label')->for('id_number') }}

                        <div class="col-md-10">
                            {{ html()->text('id_number')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.branches.id_number'))
                                ->attribute('maxlength', 190)
                                ->value(old('id_number',$data->id_number))
                                 }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.branches.commercial_number'))->class('col-md-2 form-control-label')->for('commercial_number') }}

                        <div class="col-md-10">
                            {{ html()->text('commercial_number')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.branches.commercial_number'))
                                ->attribute('maxlength', 190)
                                ->value(old('commercial_number',$data->commercial_number))
                                 }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.branches.tax_card_number'))->class('col-md-2 form-control-label')->for('tax_card_number') }}

                        <div class="col-md-10">
                            {{ html()->text('tax_card_number')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.branches.tax_card_number'))
                                ->attribute('maxlength', 190)
                                ->value(old('tax_card_number',$data->tax_card_number))
                                 }}
                        </div><!--col-->
                    </div><!--form-group-->


                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.branches.percentage'))->class('col-md-2 form-control-label')->for('percentage') }}

                        <div class="col-md-10">
                            {{ html()->number('percentage')
                                    ->class('form-control')
                                    ->placeholder(__('labels.backend.branches.percentage'))
                                    ->attribute('min',1)
                                    ->attribute('step','any')
                                    ->required()
                                    ->value(old('percentage',$data->percentage))
                                 }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.branches.devices'))->class('col-md-2 form-control-label')->for('devices') }}

                        <div class="col-md-10">
                            {{ html()->textarea('devices')
                                    ->class('form-control')
                                    ->placeholder(__('labels.backend.branches.devices'))
                                    ->attribute('rows',6)
                                    ->required('maxlength',500)
                                    ->value(old('devices',$data->devices))
                                 }}
                        </div><!--col-->
                    </div><!--form-group-->

                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer clearfix">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.auth.branches.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.edit')) }}
                </div><!--col-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
    {{ html()->form()->close() }}
@endsection
