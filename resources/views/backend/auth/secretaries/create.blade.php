@extends('backend.layouts.app')

@section('title', __('labels.backend.secretaries.management') . ' | ' . __('labels.backend.secretaries.create'))


@section('content')
    {{ html()->form('POST', route('admin.auth.secretaries.store'))->class('form-horizontal')->acceptsFiles()->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        @lang('labels.backend.secretaries.management')
                        <small class="text-muted">@lang('labels.backend.secretaries.create')</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr>

            <div class="row mt-4 mb-4">
                <div class="col">

                    <div class="form-group row">
                        <div class="col-md-10">
                            {{ html()->text('role')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.secretaries.role'))
                                ->attribute('hidden', true)
                                ->value('secretary')
                                 }}
                        </div><!--col-->
                    </div><!--form-group-->

{{--                    @if ($logged_in_user->role = config('access.users.branch_role'))--}}
                        <div class="form-group row">
                            <div class="col-md-10">
                                {{ html()->text('branch_id')
                                    ->class('form-control')
                                    ->placeholder(__('labels.backend.secretaries.branch_id'))
                                    ->attribute('hidden', true)
                                    ->value(3)
                                     }}
                            </div><!--col-->
                        </div><!--form-group-->
                    {{--@endif--}}

                    {{--@if($logged_in_user->isAdmin())--}}
                    {{--<div class="form-group row">--}}
                    {{--{{ html()->label(__('labels.backend.secretaries.branch'))->class('col-md-2 form-control-label')->for('branch_id') }}--}}

                    {{--<div class="col-md-10">--}}
                    {{--{{ html()->select('branch_id',['' => 'إختر الفرع'] + $branch->pluck('first_name', 'id')->toArray())--}}
                    {{--->required()--}}
                    {{--->class('form-control') }}--}}
                    {{--</div><!--col-->--}}
                    {{--</div><!--form-group-->--}}
                    {{--@endif--}}

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.secretaries.first_name'))->class('col-md-2 form-control-label')->for('first_name') }}

                        <div class="col-md-10">
                            {{ html()->text('first_name')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.secretaries.first_name'))
                                ->attribute('maxlength', 191)
                                ->required()
                                ->value(old('first_name'))
                                 }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.secretaries.last_name'))->class('col-md-2 form-control-label')->for('last_name') }}

                        <div class="col-md-10">
                            {{ html()->text('last_name')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.secretaries.last_name'))
                                ->attribute('maxlength', 191)
                                ->required()
                                ->value(old('last_name'))
                                 }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.secretaries.email'))->class('col-md-2 form-control-label')->for('email') }}

                        <div class="col-md-10">
                            {{ html()->email('email')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.secretaries.email'))
                                ->attribute('maxlength', 191)
                                ->required()
                                ->value(old('email'))
                                 }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.secretaries.password'))->class('col-md-2 form-control-label')->for('password') }}

                        <div class="col-md-10">
                            {{ html()->password('password')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.secretaries.password'))
                                ->required()
                                ->attribute('maxlength', 191)
                                 }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.secretaries.phone'))->class('col-md-2 form-control-label')->for('phone') }}

                        <div class="col-md-10">
                            {{ html()->text('phone')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.secretaries.phone'))
                                ->attribute('maxlength', 191)
                                ->required()
                                ->value(old('phone'))
                                 }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.secretaries.second_phone'))->class('col-md-2 form-control-label')->for('second_phone') }}

                        <div class="col-md-10">
                            {{ html()->text('second_phone')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.secretaries.second_phone'))
                                ->attribute('maxlength', 191)
                                ->value(old('second_phone'))
                                 }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.secretaries.address'))->class('col-md-2 form-control-label')->for('address') }}

                        <div class="col-md-10">
                            {{ html()->text('address')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.secretaries.address'))
                                ->attribute('maxlength', 500)
                                ->required()
                                ->value(old('address'))
                                 }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.secretaries.id_number'))->class('col-md-2 form-control-label')->for('id_number') }}

                        <div class="col-md-10">
                            {{ html()->text('id_number')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.secretaries.id_number'))
                                ->attribute('maxlength', 190)
                                ->required()
                                ->value(old('id_number'))
                                 }}
                        </div><!--col-->
                    </div><!--form-group-->


                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.secretaries.salary'))->class('col-md-2 form-control-label')->for('salary') }}

                        <div class="col-md-10">
                            {{ html()->number('salary')
                                    ->class('form-control')
                                    ->placeholder(__('labels.backend.secretaries.salary'))
                                    ->attribute('min',1)
                                    ->attribute('step','any')
                                    ->required()
                                    ->value(old('salary'))
                                 }}
                        </div><!--col-->
                    </div><!--form-group-->

                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer clearfix">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.auth.secretaries.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.create')) }}
                </div><!--col-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
    {{ html()->form()->close() }}
@endsection
