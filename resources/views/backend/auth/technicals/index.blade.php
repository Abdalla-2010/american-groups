@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.technicals.management'))

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        {{ __('labels.backend.technicals.management') }}
                    </h4>
                </div><!--col-->

                <div class="col-sm-7">
                    <div class="btn-toolbar float-right" role="toolbar"
                         aria-label="@lang('labels.general.toolbar_btn_groups')">
                        <a href="{{ route('admin.auth.technicals.create') }}" class="btn btn-success ml-1"
                           data-toggle="tooltip"
                           title="@lang('labels.general.create_new')"><i class="fas fa-plus-circle"></i></a>
                    </div><!--btn-toolbar-->
                </div><!--col-->
            </div><!--row-->

            <div class="row mt-4">
                <div class="col">
                    <div>
                        <table class="table" id="result-table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>@lang('labels.backend.technicals.first_name')</th>
                                <th>@lang('labels.backend.technicals.last_name')</th>
                                <th>@lang('labels.backend.technicals.email')</th>
                                <th>@lang('labels.backend.technicals.phone')</th>
                                <th>@lang('labels.backend.technicals.second_phone')</th>
                                <th>@lang('labels.backend.technicals.address')</th>
                                <th>@lang('labels.backend.technicals.salary')</th>
                                <th>@lang('labels.general.actions')</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->
    </div><!--card-->
@endsection

@push('after-scripts')
    <script>
        $dataTable('{{ route('admin.auth.technicals.index') }}', [
            {data: 'DT_RowIndex', name: 'id'},
            {data: 'first_name', name: 'first_name'},
            {data: 'last_name', name: 'last_name'},
            {data: 'email', name: 'email'},
            {data: 'phone', name: 'phone'},
            {data: 'second_phone', name: 'second_phone'},
            {data: 'address', name: 'address'},
            {data: 'salary', name: 'salary'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]);
    </script>
@endpush
