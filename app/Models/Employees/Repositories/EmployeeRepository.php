<?php

namespace App\Models\Employees\Repositories;

use App\Models\Auth\Branches\Repositories\BranchRepository;
use App\Models\Employees\Employee;
use App\Repositories\BaseRepository;
use Yajra\DataTables\DataTables;

class EmployeeRepository extends BaseRepository
{

    //    protected $with = [''];

	public function __construct(Employee $employee)
    {
        $this->model = $employee;
    }

    public function getWithDatatable()
    {
        if(auth()->user()->role == 'branch')
        {
            return DataTables::of($this->model
                ->where('branch_id',auth()->id())
                ->with($this->with)->select())
                ->addIndexColumn()
                ->addColumn('action', function ($data) {
                    return view('backend.includes.partials.datatables.action', ['action' => $this->setDatatableAction($data)])->render();
                })->make(true);
        }

        return DataTables::of($this->model->with($this->with)->select())
            ->addIndexColumn()
            ->addColumn('action', function ($data) {
                return view('backend.includes.partials.datatables.action', ['action' => $this->setDatatableAction($data)])->render();
            })->make(true);
    }


    public function createData()
    {
        return [
            'branch' => app(BranchRepository::class)->where('role','branch')->get()
        ];
    }

    public function editData($id)
    {
        $data = parent::editData($id);

        $data['branch'] = app(BranchRepository::class)->where('role','branch')->get();
        $data['employee_branch'] = app(EmployeeRepository::class)
            ->where('id',$id)
            ->where('branch_id',$data['data']->branch_id)->get()->pluck('branch_id');

        return $data;
    }

}

