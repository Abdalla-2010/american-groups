<?php

namespace App\Models\Employees\Requests;
use Illuminate\Foundation\Http\FormRequest;

class UpdateEmployeeRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        $validation['first_name'] = 'required|max:190';
        $validation['last_name'] = 'required|max:190';
        $validation['phone'] = 'required|numeric|digits:11|unique:employees,phone,' . request()->route('employee');
        $validation['second_phone'] = 'nullable|numeric|digits:11|unique:employees,second_phone,' . request()->route('employee');
        $validation['salary']= 'required|numeric';
        $validation['branch_id']= 'required';

        return $validation;

    }

    public function attributes()
    {
        return [
            'first_name' => __('labels.backend.employees.first_name'),
            'last_name' => __('labels.backend.employees.last_name'),
            'phone' => __('labels.backend.employees.phone'),
            'second_phone' => __('labels.backend.employees.second_phone'),
            'salary' => __('labels.backend.employees.salary'),
            'branch_id' => __('labels.backend.employees.branch'),
        ];
    }
}
