<?php

namespace App\Models\Employees;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasRoute;

class Employee extends Model
{
    use HasRoute;

    protected $fillable = ['first_name','last_name','phone','second_phone','salary','branch_id'];

    protected $routeName = 'admin.employees';
}


