<?php

namespace App\Models\Auth\Secretaries\Repositories;

use App\Models\Auth\Branches\Repositories\BranchRepository;
use App\Models\Auth\Secretaries\Secretary;
use App\Repositories\BaseRepository;
use Yajra\DataTables\DataTables;

class SecretaryRepository extends BaseRepository
{

    public function __construct(Secretary $secretary)
    {
        $this->model = $secretary;
    }

    public function getWithDatatable()
    {
        if(auth()->user()->role == 'branch')
        {
            return DataTables::of($this->model->where('role','secretary')
                ->where('branch_id',auth()->id())
                ->with($this->with)->select())
                ->addIndexColumn()
                ->addColumn('action', function ($data) {
                    return view('backend.includes.partials.datatables.action', ['action' => $this->setDatatableAction($data)])->render();
                })->make(true);
        }

        return DataTables::of($this->model->where('role','secretary')->with($this->with)->select())
            ->addIndexColumn()
            ->addColumn('action', function ($data) {
                return view('backend.includes.partials.datatables.action', ['action' => $this->setDatatableAction($data)])->render();
            })->make(true);
    }


    public function createData()
    {
        return [
            'branch' => app(BranchRepository::class)->where('role','branch')->get()
        ];
    }

    public function editData($id)
    {
        $data = parent::editData($id);

        $data['branch'] = app(BranchRepository::class)->where('role','branch')->get();
        $data['secretary_branch'] = app(SecretaryRepository::class)
            ->where('id',$id)
            ->where('branch_id',$data['data']->branch_id)->get()->pluck('branch_id');

        return $data;
    }


}
