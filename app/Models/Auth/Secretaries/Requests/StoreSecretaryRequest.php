<?php

namespace App\Models\Auth\Secretaries\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreSecretaryRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        $validation['first_name'] = 'required|max:190';
        $validation['last_name'] = 'required|max:190';
        $validation['email']= 'required|email|unique:users';
        $validation['password']= 'required|min:6|max:30';
        $validation['phone']= 'required|numeric|digits:11|unique:users';
        $validation['salary']= 'required|numeric';
        $validation['second_phone']= 'nullable|numeric|digits:11|unique:users';
        $validation['address']= 'required|max:500';
        $validation['id_number']= 'required|numeric|digits:14|unique:users';
//        $validation['branch_id'] = 'required';


        return $validation;

    }

    public function attributes()
    {
        return [
            'first_name' => __('labels.backend.secretaries.first_name'),
            'last_name' => __('labels.backend.secretaries.last_name'),
            'email' => __('labels.backend.secretaries.email'),
            'password' => __('labels.backend.secretaries.password'),
            'phone' => __('labels.backend.secretaries.phone'),
            'salary' => __('labels.backend.secretaries.salary'),
            'second_phone' => __('labels.backend.secretaries.second_phone'),
            'address' => __('labels.backend.secretaries.address'),
            'id_number' => __('labels.backend.secretaries.id_number'),
            'branch_id' => __('labels.backend.secretaries.branch'),

        ];
    }
}
