<?php

namespace App\Models\Auth\Secretaries;

use App\Models\Auth\User;
use App\Traits\HasRoute;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;


class Secretary extends User
{
    use HasRoute;

    protected $table = 'users';

    public $routeName = 'admin.auth.secretaries';

    protected $fillable = [
        'first_name','last_name','email','password','phone',
        'salary','role','id_number','second_phone','address','branch_id'
    ];



}
