<?php

namespace App\Models\Auth\Carriers\Repositories;

use App\Models\Auth\Branches\Repositories\BranchRepository;
use App\Models\Auth\Carriers\Carrier;
use App\Repositories\BaseRepository;
use Yajra\DataTables\DataTables;

class CarrierRepository extends BaseRepository
{

    public function __construct(Carrier $carriers)
    {
        $this->model = $carriers;
    }

    public function getWithDatatable()
    {

        if(auth()->user()->role == 'branch')
        {
            return DataTables::of($this->model->where('role','carrier')
                ->where('branch_id',auth()->id())
                ->with($this->with)->select())
                ->addIndexColumn()
                ->addColumn('action', function ($data) {
                    return view('backend.includes.partials.datatables.action', ['action' => $this->setDatatableAction($data)])->render();
                })->make(true);
        }

        return DataTables::of($this->model->where('role','carrier')->with($this->with)->select())
            ->addIndexColumn()
            ->addColumn('action', function ($data) {
                return view('backend.includes.partials.datatables.action', ['action' => $this->setDatatableAction($data)])->render();
            })->make(true);
    }


    public function createData()
    {
        return [
            'branch' => app(BranchRepository::class)->where('role','branch')->get()
        ];
    }

    public function editData($id)
    {
        $data = parent::editData($id);

        $data['branch'] = app(BranchRepository::class)->where('role','branch')->get();
        $data['carrier_branch'] = app(CarrierRepository::class)
            ->where('id',$id)
            ->where('branch_id',$data['data']->branch_id)->get()->pluck('branch_id');

        return $data;
    }


}
