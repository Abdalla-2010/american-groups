<?php

namespace App\Models\Auth\Carriers\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCarrierRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        $validation['first_name'] = 'required|max:190';
        $validation['last_name'] = 'required|max:190';
        $validation['email'] = 'required|email|unique:users,email,' . request()->route('carrier');
        $validation['password'] = 'nullable|min:6|max:30';
        $validation['phone'] = 'required|numeric|digits:11|unique:users,phone,' . request()->route('carrier');
        $validation['salary']= 'required|numeric';
        $validation['devices']= 'required|max:500';
        $validation['branch_id'] = 'required';


        return $validation;

    }

    public function attributes()
    {
        return [
            'first_name' => __('labels.backend.carriers.first_name'),
            'last_name' => __('labels.backend.carriers.last_name'),
            'email' => __('labels.backend.carriers.email'),
            'password' => __('labels.backend.carriers.password'),
            'phone' => __('labels.backend.carriers.phone'),
            'salary' => __('labels.backend.carriers.salary'),
            'devices' => __('labels.backend.carriers.devices'),
            'branch_id' => __('labels.backend.carriers.branch'),

        ];
    }
}
