<?php

namespace App\Models\Auth\Carriers;

use App\Models\Auth\User;
use App\Traits\HasRoute;

class Carrier extends User
{
    use HasRoute;

    protected $table = 'users';

    public $routeName = 'admin.auth.carriers';

    protected $fillable = ['first_name','last_name','email','password','phone','salary','role','devices','branch_id'];


}
