<?php

namespace App\Models\Auth\Branches;

use App\Models\Auth\User;
use App\Traits\HasRoute;

class Branch extends User
{
    use HasRoute;

    protected $table = 'users';

    public $routeName = 'admin.auth.branches';

    protected $fillable = [
        'first_name','address','email','password','phone','second_phone','role',
        'id_number','commercial_number','tax_card_number','devices','percentage',

    ];


}
