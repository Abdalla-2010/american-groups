<?php

namespace App\Models\Auth\Branches\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBranchRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        $validation['first_name'] = 'required|max:190';
        $validation['email'] = 'required|email|unique:users,email,' . request()->route('branch');
        $validation['password'] = 'nullable|min:6|max:30';
        $validation['phone'] = 'required|numeric|digits:11|unique:users,phone,' . request()->route('branch');
        $validation['second_phone'] = 'nullable|numeric|digits:11|unique:users,second_phone,' . request()->route('branch');
        $validation['address'] = 'required|max:500';
        $validation['devices']= 'required|max:500';
        $validation['id_number']= 'required|numeric|digits:14|unique:users,id_number,' . request()->route('branch');
        $validation['commercial_number']= 'required|numeric|unique:users,commercial_number,' . request()->route('branch');
        $validation['tax_card_number']= 'required|numeric|unique:users,tax_card_number,' . request()->route('branch');
        $validation['percentage']= 'required|numeric|max:100|min:0';

        return $validation;

    }

    public function attributes()
    {
        return [
            'first_name' => __('labels.backend.branches.first_name'),
            'email' => __('labels.backend.branches.email'),
            'password' => __('labels.backend.branches.password'),
            'phone' => __('labels.backend.branches.phone'),
            'second_phone' => __('labels.backend.branches.second_phone'),
            'address' => __('labels.backend.branches.address'),
            'devices' => __('labels.backend.branches.devices'),
            'id_number' => __('labels.backend.branches.id_number'),
            'commercial_number' => __('labels.backend.branches.commercial_number'),
            'tax_card_number' => __('labels.backend.branches.tax_card_number'),
            'percentage' => __('labels.backend.branches.percentage'),
        ];
    }
}
