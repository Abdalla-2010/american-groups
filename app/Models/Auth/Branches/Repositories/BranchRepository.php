<?php

namespace App\Models\Auth\Branches\Repositories;

use App\Models\Auth\Branches\Branch;
use App\Repositories\BaseRepository;
use Yajra\DataTables\DataTables;

class BranchRepository extends BaseRepository
{

    public function __construct(Branch $branches)
    {
        $this->model = $branches;
    }

    public function setDatatableAction($data)
    {
        if ($data->id != 3) {

            $action_links = '';

            if ($this->hasEdit) {
                $action_links .= "<a href='{$data->url->edit}' class='btn btn-primary'>
                                    <i class='fas fa-edit' data-toggle='tooltip' data-placement='top' title='' data-original-title='Edit'></i>
                                </a>";
            }

            if ($this->hasDelete) {
                $action_links .= "<a href='{$data->url->delete}' 
                                     data-method='delete'
                                     data-trans-button-cancel='" . __('buttons.general.cancel') . "'
                                     data-trans-button-confirm='" . __('buttons.general.crud.delete') . "'
                                     data-trans-title='" . __('strings.backend.general.are_you_sure') . "'                     
                                     class='btn btn-danger'>
                        
                                     <i class='fas fa-trash' data-toggle='tooltip' 
                                     data-placement='top' title='" . __('buttons.general.crud.delete') . "'></i>
                                     
                                </a>";

            }

            return $action_links;
        }
        return '';
    }

    public function getWithDatatable()
    {
        return DataTables::of($this->model->where('role', 'branch')->with($this->with)->select())
            ->addIndexColumn()
            ->addColumn('action', function ($data) {
                return view('backend.includes.partials.datatables.action', ['action' => $this->setDatatableAction($data)])->render();
            })->make(true);
    }


    public function createData()
    {
        return [
        ];
    }

    public function editData($id)
    {
        $data = parent::editData($id);

        return $data;
    }


}
