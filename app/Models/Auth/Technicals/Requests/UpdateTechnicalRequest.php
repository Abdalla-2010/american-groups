<?php

namespace App\Models\Auth\Technicals\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTechnicalRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        $validation['first_name'] = 'required|max:190';
        $validation['last_name'] = 'required|max:190';
        $validation['email'] = 'required|email|unique:users,email,' . request()->route('technical');
        $validation['password'] = 'nullable|min:6|max:30';
        $validation['phone'] = 'required|numeric|digits:11|unique:users,phone,' . request()->route('technical');
        $validation['salary']= 'required|numeric';
        $validation['devices']= 'required|max:500';
        $validation['second_phone'] = 'nullable|numeric|digits:11|unique:users,second_phone,' . request()->route('technical');
        $validation['address'] = 'required|max:500';
        $validation['id_number']= 'required|numeric|digits:14|unique:users,id_number,' . request()->route('technical');
        $validation['branch_id'] = 'required';


        return $validation;

    }

    public function attributes()
    {
        return [
            'first_name' => __('labels.backend.technicals.first_name'),
            'last_name' => __('labels.backend.technicals.last_name'),
            'email' => __('labels.backend.technicals.email'),
            'password' => __('labels.backend.technicals.password'),
            'phone' => __('labels.backend.technicals.phone'),
            'salary' => __('labels.backend.technicals.salary'),
            'devices' => __('labels.backend.technicals.devices'),
            'second_phone' => __('labels.backend.secretaries.second_phone'),
            'address' => __('labels.backend.secretaries.address'),
            'id_number' => __('labels.backend.secretaries.id_number'),
            'branch_id' => __('labels.backend.secretaries.branch'),
        ];
    }
}
