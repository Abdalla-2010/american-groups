<?php

namespace App\Models\Auth\Technicals;

use App\Models\Auth\User;
use App\Traits\HasRoute;

class Technical extends User
{
    use HasRoute;

    protected $table = 'users';

    public $routeName = 'admin.auth.technicals';

    protected $fillable = [
        'first_name','last_name','email','password','address',
        'phone','salary','role','devices','second_phone','id_number','branch_id'
    ];


}
