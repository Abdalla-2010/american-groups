<?php

namespace App\Models\Auth\Technicals\Repositories;

use App\Models\Auth\Branches\Repositories\BranchRepository;
use App\Models\Auth\Technicals\Technical;
use App\Repositories\BaseRepository;
use Yajra\DataTables\DataTables;

class TechnicalRepository extends BaseRepository
{

    public function __construct(Technical $Technicals)
    {
        $this->model = $Technicals;
    }

    public function getWithDatatable()
    {
        if(auth()->user()->role == 'branch')
        {
            return DataTables::of($this->model->where('role','technical')
                ->where('branch_id',auth()->id())
                ->with($this->with)->select())
                ->addIndexColumn()
                ->addColumn('action', function ($data) {
                    return view('backend.includes.partials.datatables.action', ['action' => $this->setDatatableAction($data)])->render();
                })->make(true);
        }

        return DataTables::of($this->model->where('role','technical')->with($this->with)->select())
            ->addIndexColumn()
            ->addColumn('action', function ($data) {
                return view('backend.includes.partials.datatables.action', ['action' => $this->setDatatableAction($data)])->render();
            })->make(true);
    }


    public function createData()
    {
        return [
            'branch' => app(BranchRepository::class)->where('role','branch')->get()
        ];
    }

    public function editData($id)
    {
        $data = parent::editData($id);

        $data['branch'] = app(BranchRepository::class)->where('role','branch')->get();
        $data['technical_branch'] = app(TechnicalRepository::class)
            ->where('id',$id)
            ->where('branch_id',$data['data']->branch_id)->get()->pluck('branch_id');

        return $data;
    }

}
