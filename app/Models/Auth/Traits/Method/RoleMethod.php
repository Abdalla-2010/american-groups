<?php

namespace App\Models\Auth\Traits\Method;

/**
 * Trait RoleMethod.
 */
trait RoleMethod
{
    /**
     * @return mixed
     */
    public function isAdmin()
    {
        return $this->name === config('access.users.admin_role');
    }

//    public function isSecretary()
//    {
//        return $this->name === config('access.users.secretary_role');
//    }
//
//    public function isBranch()
//    {
//        return $this->name === config('access.users.branch_role');
//    }

}
