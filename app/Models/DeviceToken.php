<?php

namespace App;

use App\Models\Auth\User;
use Illuminate\Database\Eloquent\Model;

class DeviceToken extends Model
{
    protected $table = 'fcm_tokens';

    protected $fillable = ['token','device'];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }


}
