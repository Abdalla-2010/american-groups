<?php

namespace App\Models\Orders\Requests;
use Illuminate\Foundation\Http\FormRequest;

class UpdateOrderRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        if (request()->has('technical_id') && request()->has('branch_id')) {

            $validation['branch_id'] = 'required';
            $validation['technical_id'] = 'required';

        }elseif(request()->has('carrier_id')){

            $validation['carrier_id'] = 'required';

        }else{

            $validation['customer_name'] = 'required|max:190';
            $validation['phone']= 'required|numeric|digits:11';
            $validation['second_phone']= 'nullable|numeric|digits:11';
            $validation['city_id']= 'required|exists:cities,id';
            $validation['description']= 'required|max:500';
            $validation['device_type']= 'required|max:190';
            $validation['device_model']= 'required|max:190';
        }

        return $validation;

    }

    public function attributes()
    {
        return [
            'customer_name' => __('labels.backend.orders.customer_name'),
            'phone' => __('labels.backend.orders.phone'),
            'second_phone' => __('labels.backend.orders.second_phone'),
            'city_id' => __('labels.backend.orders.city'),
            'description' => __('labels.backend.orders.description'),
            'device_type' => __('labels.backend.orders.device_type'),
            'device_model' => __('labels.backend.orders.device_model'),
        ];
    }
}
