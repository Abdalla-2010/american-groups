<?php

namespace App\Models\Orders\Repositories;

use App\Http\Requests\Api\OrderTechResponse;
use App\Models\Cities\Repositories\CityRepository;
use App\Models\Orders\Order;
use App\Repositories\Backend\Auth\UserRepository;
use App\Repositories\BaseRepository;
use Yajra\DataTables\DataTables;

class OrderRepository extends BaseRepository
{

    protected $with = ['city','technical','branch'];

	public function __construct(Order $order)
    {
        $this->model = $order;
    }

    public function getWithDatatable()
    {
        if(auth()->user()->role == 'branch')
        {
            return DataTables::of($this->model
                ->where('branch_id',auth()->id())
                ->with($this->with)->select())
                ->addIndexColumn()
                ->addColumn('action', function ($data) {
                    return view('backend.includes.partials.datatables.action', ['action' => $this->setDatatableAction($data)])->render();
                })->make(true);
        }

        return DataTables::of($this->model->with($this->with)->select())
            ->addIndexColumn()
            ->addColumn('action', function ($data) {
                return view('backend.includes.partials.datatables.action', ['action' => $this->setDatatableAction($data)])->render();
            })->make(true);
    }

    public function apiMakeOrder($request)
    {
        $this->model->create($request->all());

        return jsonResponse([
            'message' => 'تم إرسال طلبك بنجاح وسيتم التواصل.'
        ]);
    }

    public function apiTechResponse($id)
    {
        if($this->model->findOrFail($id)->status > 2){
            return jsonResponse([
                'message' => 'عفوا لقد أرسلت ردك من قبل ولا يمكن تنفيذ هذا الإجراء الآن .'
            ]);
        }
        $this->model->findOrFail($id)->update(request()->all());

        return jsonResponse([
            'message' => 'تم إرسال ردك بنجاح وسيتم التواصل.'
        ]);
    }

    public function apiGetMyOrder()
    {
        $data = $this->model->where('technical_id',auth()->id())->orderBy('id','desc')->get();

        return jsonResponse([
            'data' => $data
        ]);
    }


    public function createData()
    {
        return [
            'city' => app(CityRepository::class)->all(),
            'technical' => app(UserRepository::class)->where('role','technical')->get(),
            'branch' => app(UserRepository::class)->where('role','branch')->get(),
        ];
    }

    public function editData($id)
    {
        $data = parent::editData($id);

        $data['city'] = app(CityRepository::class)->all();

        $data['technical'] = app(UserRepository::class)->where('role','technical')->get();
        $data['order_technical'] = $data['data']->technical()->get()->pluck('id');

        $data['branch'] = app(UserRepository::class)->where('role','branch')->get();
        $data['order_branch'] = $data['data']->branch()->get()->pluck('id');

        $data['carrier'] = app(UserRepository::class)->where('role','carrier')->get();
        $data['order_carrier'] = $data['data']->carrier()->get()->pluck('id');

        return $data;
    }

}

