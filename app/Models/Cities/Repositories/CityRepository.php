<?php

namespace App\Models\Cities\Repositories;

use App\Models\Cities\City;
use App\Repositories\BaseRepository;

class CityRepository extends BaseRepository
{

    //    protected $with = [''];

	public function __construct(City $city)
    {
        $this->model = $city;
    }


    public function createData()
    {
        return [
//            'example' => app(ExampleRepository::class)->all(),
        ];
    }

    public function editData($id)
    {
        $data = parent::editData($id);

//        $data['example'] = app(ExampleRepository::class)->all();

        return $data;
    }

}

