<?php

namespace App\Models\Cities;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasRoute;

class City extends Model
{
    use HasRoute;

    protected $fillable = ['name'];

    protected $routeName = 'admin.cities';
}


