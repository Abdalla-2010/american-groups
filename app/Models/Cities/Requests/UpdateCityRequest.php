<?php

namespace App\Models\Cities\Requests;
use Illuminate\Foundation\Http\FormRequest;

class UpdateCityRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        $validation['name'] = 'required|max:190|unique:cities,name,' . request()->route('city');

        return $validation;

    }

    public function attributes()
    {
        return [
            'name' => __('labels.backend.cities.name'),
        ];
    }
}
