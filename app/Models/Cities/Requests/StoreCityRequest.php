<?php

namespace App\Models\Cities\Requests;
use Illuminate\Foundation\Http\FormRequest;

class StoreCityRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        $validation['name'] = 'required|max:190|unique:cities,name';

        return $validation;

    }

    public function attributes()
    {
        return [
            'name' => __('labels.backend.cities.name'),
        ];
    }
}
