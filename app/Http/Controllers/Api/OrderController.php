<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\OrderRequest;
use App\Http\Requests\Api\OrderTechResponse;
use App\Models\Orders\Repositories\OrderRepository;


class OrderController extends Controller
{

    private $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function index()
    {
        return $this->orderRepository->apiGetMyOrder();
    }

    public function store(OrderRequest $request)
    {
        return $this->orderRepository->apiMakeOrder($request);
    }

    public function update($id,OrderTechResponse $request)
    {
        return $this->orderRepository->apiTechResponse($id);
    }

}
