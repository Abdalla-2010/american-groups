<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\LoginRequest;
use App\Repositories\Backend\Auth\UserRepository;


class LoginController extends Controller
{

    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function index(LoginRequest $request)
    {
        return $this->userRepository->apiLogin($request);
    }
}