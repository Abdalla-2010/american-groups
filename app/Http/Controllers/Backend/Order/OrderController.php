<?php

namespace App\Http\Controllers\Backend\Order;

use App\Http\Controllers\Backend\CustomController;
use App\Models\Orders\Repositories\OrderRepository;
use App\Models\Orders\Requests\StoreOrderRequest;
use App\Models\Orders\Requests\UpdateOrderRequest;


class OrderController extends CustomController
{
    protected $view = 'backend.orders';

    protected $route = 'admin.orders';

    protected $storeRequestFile = StoreOrderRequest::class;

    protected $updateRequestFile = UpdateOrderRequest::class;

    public function __construct(OrderRepository $repository)
    {
        parent::__construct($repository);
    }

    public function print_order($id)
    {
        $data = $this->model->getById($id)->load('city');

        return view($this->view . '.print',compact('data'));
    }

    public function technicalAssignment($id)
    {
        if ($this->model->getById($id)->technical_id == null) {

            return view($this->view . '.technical-assignment')->with($this->model->editData($id));
        }

        return abort(404);
    }

    public function carrierAssignment($id)
    {
        if ($this->model->getById($id)->carrier_id == null) {

            return view($this->view . '.carrier-assignment')->with($this->model->editData($id));
        }

        return abort(404);
    }


}


