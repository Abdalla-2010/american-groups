<?php

namespace App\Http\Controllers\Backend\Employee;

use App\Http\Controllers\Backend\CustomController;
use App\Models\Employees\Repositories\EmployeeRepository;
use App\Models\Employees\Requests\StoreEmployeeRequest;
use App\Models\Employees\Requests\UpdateEmployeeRequest;


class EmployeeController extends CustomController
{
    protected $view = 'backend.employees';

    protected $route = 'admin.employees';

    protected $storeRequestFile = StoreEmployeeRequest::class;

    protected $updateRequestFile = UpdateEmployeeRequest::class;

    public function __construct(EmployeeRepository $repository)
    {
        parent::__construct($repository);
    }

}


