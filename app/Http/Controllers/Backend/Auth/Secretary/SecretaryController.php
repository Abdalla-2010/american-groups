<?php

namespace App\Http\Controllers\Backend\Auth\Secretary;

use App\Http\Controllers\Backend\CustomController;
use App\Models\Auth\Secretaries\Repositories\SecretaryRepository;
use App\Models\Auth\Secretaries\Requests\StoreSecretaryRequest;
use App\Models\Auth\Secretaries\Requests\UpdateSecretaryRequest;

class SecretaryController extends CustomController
{
    protected $view = 'backend.auth.secretaries';

    protected $storeRequestFile = StoreSecretaryRequest::class;

    protected $updateRequestFile = UpdateSecretaryRequest::class;

    protected $route = 'admin.auth.secretaries';

    public function __construct(SecretaryRepository $repository)
    {
        parent::__construct($repository);
    }

    public function index()
    {
        return app($this->indexResponse, ['model' => $this->model, 'view' => $this->view]);
    }
}
