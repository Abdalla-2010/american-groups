<?php

namespace App\Http\Controllers\Backend\Auth\Technical;

use App\Http\Controllers\Backend\CustomController;
use App\Models\Auth\Technicals\Repositories\TechnicalRepository;
use App\Models\Auth\Technicals\Requests\StoreTechnicalRequest;
use App\Models\Auth\Technicals\Requests\UpdateTechnicalRequest;

class TechnicalController extends CustomController
{
    protected $view = 'backend.auth.technicals';

    protected $storeRequestFile = StoreTechnicalRequest::class;

    protected $updateRequestFile = UpdateTechnicalRequest::class;

    protected $route = 'admin.auth.technicals';

    public function __construct(TechnicalRepository $repository)
    {
        parent::__construct($repository);
    }

    public function index()
    {
        return app($this->indexResponse, ['model' => $this->model, 'view' => $this->view]);
    }
}
