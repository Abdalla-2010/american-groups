<?php

namespace App\Http\Controllers\Backend\Auth\Branch;

use App\Http\Controllers\Backend\CustomController;
use App\Models\Auth\Branches\Repositories\BranchRepository;
use App\Models\Auth\Branches\Requests\StoreBranchRequest;
use App\Models\Auth\Branches\Requests\UpdateBranchRequest;

class BranchController extends CustomController
{
    protected $view = 'backend.auth.branches';

    protected $storeRequestFile = StoreBranchRequest::class;

    protected $updateRequestFile = UpdateBranchRequest::class;

    protected $route = 'admin.auth.branches';

    public function __construct(BranchRepository $repository)
    {
        parent::__construct($repository);
    }

    public function index()
    {
        return app($this->indexResponse, ['model' => $this->model, 'view' => $this->view]);
    }
}
