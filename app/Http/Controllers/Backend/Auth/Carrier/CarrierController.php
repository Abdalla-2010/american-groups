<?php

namespace App\Http\Controllers\Backend\Auth\Carrier;

use App\Http\Controllers\Backend\CustomController;
use App\Models\Auth\Carriers\Repositories\CarrierRepository;
use App\Models\Auth\Carriers\Requests\StoreCarrierRequest;
use App\Models\Auth\Carriers\Requests\UpdateCarrierRequest;

class CarrierController extends CustomController
{
    protected $view = 'backend.auth.carriers';

    protected $storeRequestFile = StoreCarrierRequest::class;

    protected $updateRequestFile = UpdateCarrierRequest::class;

    protected $route = 'admin.auth.carriers';

    public function __construct(CarrierRepository $repository)
    {
        parent::__construct($repository);
    }

    public function index()
    {
        return app($this->indexResponse, ['model' => $this->model, 'view' => $this->view]);
    }
}
