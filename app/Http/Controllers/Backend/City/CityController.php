<?php

namespace App\Http\Controllers\Backend\City;

use App\Http\Controllers\Backend\CustomController;
use App\Models\Cities\Repositories\CityRepository;
use App\Models\Cities\Requests\StoreCityRequest;
use App\Models\Cities\Requests\UpdateCityRequest;


class CityController extends CustomController
{
    protected $view = 'backend.cities';

    protected $route = 'admin.cities';

    protected $storeRequestFile = StoreCityRequest::class;

    protected $updateRequestFile = UpdateCityRequest::class;

    public function __construct(CityRepository $repository)
    {
        parent::__construct($repository);
    }

}


