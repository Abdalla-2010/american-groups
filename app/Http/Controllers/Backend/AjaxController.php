<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Auth\User;
use App\Repositories\Backend\Auth\UserRepository;

class AjaxController extends Controller
{


    public function index()
    {
        $func = $_POST['method'];
        return $this->$func(json_decode($_POST['oVals'], true));
    }

    public function ajaxCall($script)
    {
        echo "<script>" . $script . "</script>";
    }



    public function getTechnicals($oVals)
    {
        $data = app(UserRepository::class)->where('branch_id',$oVals['id'])
            ->where('role','technical')
            ->get();

        $result = '<option value="" selected disabled>إختر فنى</option>';
        foreach ($data as $value) {
            $result .= '<option value="' . $value->id . '">' . $value->first_name . ' ' . $value->last_name . '</option>';
        };
        $this->ajaxCall("$('#technical').html('$result');");
        $this->ajaxCall("$('#technical').selectpicker('refresh');");

    }

    public function getCarriers($oVals)
    {
        $data = app(UserRepository::class)->where('branch_id',$oVals['id'])
            ->where('role','carrier')
            ->get();

        $result = '<option value="" selected disabled>إختر سحّاب</option>';
        foreach ($data as $value) {
            $result .= '<option value="' . $value->id . '">' . $value->first_name . ' ' . $value->last_name . '</option>';
        };
        $this->ajaxCall("$('#carrier').html('$result');");
        $this->ajaxCall("$('#carrier').selectpicker('refresh');");

    }






}
