<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StudentRegisterRequest.
 */
class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'password' => 'required',
            'token' => 'required',
            'device' => 'required|in:android,ios',
        ];
    }

    public function attributes()
    {
        return [
            'email' => __('labels.api.users.email'),
            'password' => __('labels.api.users.password'),
        ];
    }
}
