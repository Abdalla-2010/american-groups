<?php

namespace App\Http\Requests\Api;
use Illuminate\Foundation\Http\FormRequest;

class OrderTechResponse extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        $validation['status'] = 'required';
        $validation['insurance_start_date']= 'required_if:status,2|date|date_format:Y-m-d';
        $validation['insurance_end_date']= 'required_if:status,2|date|date_format:Y-m-d|after:insurance_start_date';

        return $validation;

    }

    public function attributes()
    {
        return [
            'status' => __('labels.api.orders.status'),
            'insurance_start_date' => __('labels.api.orders.insurance_start_date'),
            'insurance_end_date' => __('labels.api.orders.insurance_end_date'),
        ];
    }
}
