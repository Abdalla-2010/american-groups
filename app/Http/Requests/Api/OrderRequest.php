<?php

namespace App\Http\Requests\Api;
use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        $validation['customer_name'] = 'required|max:190';
        $validation['phone']= 'required|numeric|digits:11';
        $validation['second_phone']= 'nullable|numeric|digits:11';
        $validation['city_id']= 'required|exists:cities,id';
        $validation['description']= 'required|max:500';
        $validation['device_type']= 'required|max:190';
        $validation['device_model']= 'required|max:190';

        return $validation;

    }

    public function attributes()
    {
        return [
            'customer_name' => __('labels.api.orders.customer_name'),
            'phone' => __('labels.api.orders.phone'),
            'second_phone' => __('labels.api.orders.second_phone'),
            'city_id' => __('labels.api.orders.city'),
            'description' => __('labels.api.orders.description'),
            'device_type' => __('labels.api.orders.device_type'),
            'device_model' => __('labels.api.orders.device_model'),
        ];
    }
}
