/*
SQLyog Ultimate v11.5 (64 bit)
MySQL - 10.1.36-MariaDB : Database - maintenance
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`maintenance` /*!40100 DEFAULT CHARACTER SET utf8 */;

/*Table structure for table `cache` */

DROP TABLE IF EXISTS `cache`;

CREATE TABLE `cache` (
  `key` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiration` int(11) NOT NULL,
  UNIQUE KEY `cache_key_unique` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `cache` */

/*Table structure for table `cities` */

DROP TABLE IF EXISTS `cities`;

CREATE TABLE `cities` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `cities` */

insert  into `cities`(`id`,`name`,`created_at`,`updated_at`) values (1,'Yousef Ahmed','2019-12-13 20:25:51','2019-12-13 20:25:51');

/*Table structure for table `employees` */

DROP TABLE IF EXISTS `employees`;

CREATE TABLE `employees` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `second_phone` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `salary` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `branch_id` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `employees` */

/*Table structure for table `failed_jobs` */

DROP TABLE IF EXISTS `failed_jobs`;

CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `failed_jobs` */

/*Table structure for table `fcm_tokens` */

DROP TABLE IF EXISTS `fcm_tokens`;

CREATE TABLE `fcm_tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `device` enum('android','ios') COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('student','teacher') COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fcm_tokens_user_id_foreign` (`user_id`),
  CONSTRAINT `fcm_tokens_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `fcm_tokens` */

insert  into `fcm_tokens`(`id`,`user_id`,`device`,`type`,`token`,`created_at`,`updated_at`) values (1,4,'ios','student','m','2019-11-09 14:41:02','2019-11-09 14:41:02');

/*Table structure for table `jobs` */

DROP TABLE IF EXISTS `jobs`;

CREATE TABLE `jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `queue` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) unsigned NOT NULL,
  `reserved_at` int(10) unsigned DEFAULT NULL,
  `available_at` int(10) unsigned NOT NULL,
  `created_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_queue_index` (`queue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `jobs` */

/*Table structure for table `ledgers` */

DROP TABLE IF EXISTS `ledgers`;

CREATE TABLE `ledgers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_type` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `recordable_type` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `recordable_id` bigint(20) unsigned NOT NULL,
  `context` tinyint(3) unsigned NOT NULL,
  `event` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `properties` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `modified` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pivot` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `extra` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` text COLLATE utf8mb4_unicode_ci,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signature` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ledgers_recordable_type_recordable_id_index` (`recordable_type`,`recordable_id`),
  KEY `ledgers_user_id_user_type_index` (`user_id`,`user_type`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `ledgers` */

insert  into `ledgers`(`id`,`user_type`,`user_id`,`recordable_type`,`recordable_id`,`context`,`event`,`properties`,`modified`,`pivot`,`extra`,`url`,`ip_address`,`user_agent`,`signature`,`created_at`,`updated_at`) values (1,'App\\Models\\Auth\\User',1,'App\\Models\\Auth\\User',1,4,'updated','{\"id\":1,\"uuid\":\"ae794921-6ac8-4830-a19f-5e4d1fa941ec\",\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"admin@admin.com\",\"avatar_type\":\"gravatar\",\"avatar_location\":null,\"password\":\"$2y$10$5xcOcqnLu.k1kTSLJFJpfett7yeHERKkVTlwiTPRPRIY4hiaJuIrq\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"9e1c0ec6a3e792ea1a4c2191f4e38d7e\",\"confirmed\":1,\"timezone\":null,\"last_login_at\":null,\"last_login_ip\":null,\"to_be_logged_out\":0,\"remember_token\":\"l6rJekmlKDz3ueg1pb15EW7fWxJ8EQ9IyfTj4lPclHdSpga6qgy8sTkq93k4\",\"created_at\":\"2019-11-09 13:53:28\",\"updated_at\":\"2019-11-09 13:53:28\",\"deleted_at\":null,\"role\":\"admin\",\"salary\":null,\"devices\":null,\"phone\":null,\"second_phone\":null,\"id_number\":null,\"commercial_number\":null,\"tax_card_number\":null,\"address\":null,\"percentage\":null,\"branch_id\":null}','[\"remember_token\"]','[]','[]','http://localhost/maintenance/public/login','::1','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.87 Safari/537.36','b12f6ea526de5e8d4041c14489199db919e5a3d6db1005516feab8230605e4962930ebbaf2df5386b29e593bf27cdbd908f1eb4b0954abb7456a2b45fe8cffdb','2019-11-09 14:08:44','2019-11-09 14:08:44'),(2,'App\\Models\\Auth\\User',1,'App\\Models\\Auth\\User',1,4,'updated','{\"id\":1,\"uuid\":\"ae794921-6ac8-4830-a19f-5e4d1fa941ec\",\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"admin@admin.com\",\"avatar_type\":\"gravatar\",\"avatar_location\":null,\"password\":\"$2y$10$5xcOcqnLu.k1kTSLJFJpfett7yeHERKkVTlwiTPRPRIY4hiaJuIrq\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"9e1c0ec6a3e792ea1a4c2191f4e38d7e\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"last_login_at\":\"2019-11-09 14:08:45\",\"last_login_ip\":\"::1\",\"to_be_logged_out\":0,\"remember_token\":\"l6rJekmlKDz3ueg1pb15EW7fWxJ8EQ9IyfTj4lPclHdSpga6qgy8sTkq93k4\",\"created_at\":\"2019-11-09 13:53:28\",\"updated_at\":\"2019-11-09 14:08:46\",\"deleted_at\":null,\"role\":\"admin\",\"salary\":null,\"devices\":null,\"phone\":null,\"second_phone\":null,\"id_number\":null,\"commercial_number\":null,\"tax_card_number\":null,\"address\":null,\"percentage\":null,\"branch_id\":null}','[\"timezone\",\"last_login_at\",\"last_login_ip\",\"updated_at\"]','[]','[]','http://localhost/maintenance/public/login','::1','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.87 Safari/537.36','4642f7e46114f45d2dbf902d9015465499862cd43a9fc1a268012c49192473af79e0e4e7075b2baea0d7cb13456a4f9527c90e8be6f9460ce27e29d6b3dc9d57','2019-11-09 14:08:46','2019-11-09 14:08:46'),(3,'App\\Models\\Auth\\User',1,'App\\Models\\Auth\\Technicals\\Technical',4,4,'created','{\"role\":\"technical\",\"branch_id\":\"3\",\"first_name\":\"vdsvds\",\"last_name\":\"ahmed\",\"email\":\"yousef.ahmed.mohammed5030@gmail.com\",\"password\":\"$2y$10$Yjfvw3ECSvpqKrWHsqsLL.ALXaiEatm0JCFQy.kUUN4qgG\\/zVcSVa\",\"phone\":\"01273975420\",\"second_phone\":null,\"address\":\"address example\",\"id_number\":\"29409151312017\",\"salary\":\"2000\",\"devices\":\"nnnn\",\"uuid\":\"4d87904e-3c80-41e8-ab38-00cbae160125\",\"updated_at\":\"2019-11-09 14:09:41\",\"created_at\":\"2019-11-09 14:09:41\",\"id\":4}','[\"role\",\"branch_id\",\"first_name\",\"last_name\",\"email\",\"password\",\"phone\",\"second_phone\",\"address\",\"id_number\",\"salary\",\"devices\",\"uuid\",\"updated_at\",\"created_at\",\"id\"]','[]','[]','http://localhost/maintenance/public/admin/auth/technicals','::1','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.87 Safari/537.36','03f2dede0175666840c2c88df6d612c23de8940a8fdeccca272fafba5c76bd67b937b69ae0583cb17e2df7c432f7cee57cc13796c532dec7124ec3be88c5bb4b','2019-11-09 14:09:41','2019-11-09 14:09:41'),(4,'App\\Models\\Auth\\User',1,'App\\Models\\Auth\\User',1,4,'updated','{\"id\":1,\"uuid\":\"ae794921-6ac8-4830-a19f-5e4d1fa941ec\",\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"admin@admin.com\",\"avatar_type\":\"gravatar\",\"avatar_location\":null,\"password\":\"$2y$10$5xcOcqnLu.k1kTSLJFJpfett7yeHERKkVTlwiTPRPRIY4hiaJuIrq\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"9e1c0ec6a3e792ea1a4c2191f4e38d7e\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"last_login_at\":\"2019-12-13 20:22:02\",\"last_login_ip\":\"::1\",\"to_be_logged_out\":0,\"remember_token\":\"l6rJekmlKDz3ueg1pb15EW7fWxJ8EQ9IyfTj4lPclHdSpga6qgy8sTkq93k4\",\"created_at\":\"2019-11-09 13:53:28\",\"updated_at\":\"2019-12-13 20:22:03\",\"deleted_at\":null,\"role\":\"admin\",\"salary\":null,\"devices\":null,\"phone\":null,\"second_phone\":null,\"id_number\":null,\"commercial_number\":null,\"tax_card_number\":null,\"address\":null,\"percentage\":null,\"branch_id\":null}','[\"last_login_at\",\"updated_at\"]','[]','[]','http://localhost/maintenance/public/login','::1','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36','0acce6fed50fedb50b87b47750846459aa07b7a127d2e486c3e0a79eb903ff558a03d2c65351daf475ee96a6a88809d4109e4609a334ccfdba5f19f1849687d8','2019-12-13 20:22:03','2019-12-13 20:22:03');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2016_06_01_000001_create_oauth_auth_codes_table',1),(4,'2016_06_01_000002_create_oauth_access_tokens_table',1),(5,'2016_06_01_000003_create_oauth_refresh_tokens_table',1),(6,'2016_06_01_000004_create_oauth_clients_table',1),(7,'2016_06_01_000005_create_oauth_personal_access_clients_table',1),(8,'2017_09_03_144628_create_permission_tables',1),(9,'2017_09_11_174816_create_social_accounts_table',1),(10,'2017_09_26_140332_create_cache_table',1),(11,'2017_09_26_140528_create_sessions_table',1),(12,'2017_09_26_140609_create_jobs_table',1),(13,'2018_04_08_033256_create_password_histories_table',1),(14,'2018_11_21_000001_create_ledgers_table',1),(15,'2019_08_19_000000_create_failed_jobs_table',1),(16,'2019_10_25_214926_add_role_column_to_users_table',1),(17,'2019_10_26_000222_add_phone_column_to_users_table',1),(18,'2019_11_01_125419_add_coulmns_to_users_table',1),(19,'2019_11_01_142531_create_employees_table',1),(20,'2019_11_02_174335_create_cities_table',1),(21,'2019_11_02_181741_create_orders_table',1),(22,'2019_11_02_195204_create_fcm_tokens_table',1),(23,'2019_11_05_220755_add_branch_id_column_to_users_table',1),(24,'2019_11_05_220951_add_branch_id_column_to_employees_table',1),(25,'2019_11_07_102903_add_branch_id_to_orders',1),(26,'2019_11_08_231256_add_columns_to_orders_table',1);

/*Table structure for table `model_has_permissions` */

DROP TABLE IF EXISTS `model_has_permissions`;

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `model_type` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_type_model_id_index` (`model_type`,`model_id`),
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `model_has_permissions` */

/*Table structure for table `model_has_roles` */

DROP TABLE IF EXISTS `model_has_roles`;

CREATE TABLE `model_has_roles` (
  `role_id` int(10) unsigned NOT NULL,
  `model_type` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_type_model_id_index` (`model_type`,`model_id`),
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `model_has_roles` */

insert  into `model_has_roles`(`role_id`,`model_type`,`model_id`) values (1,'App\\Models\\Auth\\User',1),(2,'App\\Models\\Auth\\User',2);

/*Table structure for table `oauth_access_tokens` */

DROP TABLE IF EXISTS `oauth_access_tokens`;

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) unsigned NOT NULL,
  `name` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `oauth_access_tokens` */

insert  into `oauth_access_tokens`(`id`,`user_id`,`client_id`,`name`,`scopes`,`revoked`,`created_at`,`updated_at`,`expires_at`) values ('b6007afa6cff9920d6d41e4879fe5729270d2bee558abefb3297d1f191f0442c22951482a09a6785',4,1,'TutsForWeb','[]',0,'2019-11-09 14:41:29','2019-11-09 14:41:29','2020-11-09 14:41:29');

/*Table structure for table `oauth_auth_codes` */

DROP TABLE IF EXISTS `oauth_auth_codes`;

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) unsigned NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `oauth_auth_codes` */

/*Table structure for table `oauth_clients` */

DROP TABLE IF EXISTS `oauth_clients`;

CREATE TABLE `oauth_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `oauth_clients` */

insert  into `oauth_clients`(`id`,`user_id`,`name`,`secret`,`redirect`,`personal_access_client`,`password_client`,`revoked`,`created_at`,`updated_at`) values (1,NULL,'American Groups Personal Access Client','kHIDcxa7CHlqneeF4WcleYqEl9eBC5Ojnv9QQYiJ','http://localhost',1,0,0,'2019-11-09 14:41:24','2019-11-09 14:41:24'),(2,NULL,'American Groups Password Grant Client','lg7s85YZljG68iFwefuJiCXlt847LZfPWqL9rMrw','http://localhost',0,1,0,'2019-11-09 14:41:24','2019-11-09 14:41:24');

/*Table structure for table `oauth_personal_access_clients` */

DROP TABLE IF EXISTS `oauth_personal_access_clients`;

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `oauth_personal_access_clients` */

insert  into `oauth_personal_access_clients`(`id`,`client_id`,`created_at`,`updated_at`) values (1,1,'2019-11-09 14:41:24','2019-11-09 14:41:24');

/*Table structure for table `oauth_refresh_tokens` */

DROP TABLE IF EXISTS `oauth_refresh_tokens`;

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `oauth_refresh_tokens` */

/*Table structure for table `orders` */

DROP TABLE IF EXISTS `orders`;

CREATE TABLE `orders` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `second_phone` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city_id` bigint(20) unsigned DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `device_type` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device_model` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `branch_id` bigint(20) unsigned DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1' COMMENT '1: waiting',
  `technical_id` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `orders_city_id_foreign` (`city_id`),
  KEY `orders_branch_id_foreign` (`branch_id`),
  KEY `orders_technical_id_foreign` (`technical_id`),
  CONSTRAINT `orders_branch_id_foreign` FOREIGN KEY (`branch_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `orders_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE SET NULL,
  CONSTRAINT `orders_technical_id_foreign` FOREIGN KEY (`technical_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `orders` */

insert  into `orders`(`id`,`customer_name`,`phone`,`second_phone`,`city_id`,`description`,`device_type`,`device_model`,`created_at`,`updated_at`,`branch_id`,`status`,`technical_id`) values (1,'ببب','01273975420','01225268831',1,'vvvv','vvvv','vvv','2019-12-13 20:26:29','2019-12-13 20:26:29',NULL,1,NULL);

/*Table structure for table `password_histories` */

DROP TABLE IF EXISTS `password_histories`;

CREATE TABLE `password_histories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `password` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `password_histories_user_id_foreign` (`user_id`),
  CONSTRAINT `password_histories_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_histories` */

insert  into `password_histories`(`id`,`user_id`,`password`,`created_at`,`updated_at`) values (1,1,'$2y$10$5xcOcqnLu.k1kTSLJFJpfett7yeHERKkVTlwiTPRPRIY4hiaJuIrq','2019-11-09 13:53:29','2019-11-09 13:53:29'),(2,2,'$2y$10$mVAiTr5Lmb9HMYGvkbe57OArfrn8id3u/aAvCg3Pyz7rTp9lkDvu2','2019-11-09 13:53:29','2019-11-09 13:53:29');

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `permissions` */

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `permissions` */

insert  into `permissions`(`id`,`name`,`guard_name`,`created_at`,`updated_at`) values (1,'view backend','web','2019-11-09 13:53:29','2019-11-09 13:53:29');

/*Table structure for table `role_has_permissions` */

DROP TABLE IF EXISTS `role_has_permissions`;

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`),
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `role_has_permissions` */

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `roles_name_index` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `roles` */

insert  into `roles`(`id`,`name`,`guard_name`,`created_at`,`updated_at`) values (1,'administrator','web','2019-11-09 13:53:29','2019-11-09 13:53:29'),(2,'user','web','2019-11-09 13:53:29','2019-11-09 13:53:29');

/*Table structure for table `sessions` */

DROP TABLE IF EXISTS `sessions`;

CREATE TABLE `sessions` (
  `id` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  UNIQUE KEY `sessions_id_unique` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `sessions` */

/*Table structure for table `social_accounts` */

DROP TABLE IF EXISTS `social_accounts`;

CREATE TABLE `social_accounts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `provider` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider_id` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` text COLLATE utf8mb4_unicode_ci,
  `avatar` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `social_accounts_user_id_foreign` (`user_id`),
  CONSTRAINT `social_accounts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `social_accounts` */

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar_type` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'gravatar',
  `avatar_location` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password_changed_at` timestamp NULL DEFAULT NULL,
  `active` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `confirmation_code` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `timezone` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login_at` timestamp NULL DEFAULT NULL,
  `last_login_ip` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_be_logged_out` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `role` enum('admin','secretary','branch','technical','carrier') COLLATE utf8mb4_unicode_ci NOT NULL,
  `salary` double(8,2) DEFAULT NULL,
  `devices` text COLLATE utf8mb4_unicode_ci,
  `phone` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `second_phone` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_number` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `commercial_number` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax_card_number` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `percentage` decimal(4,2) DEFAULT NULL,
  `branch_id` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`uuid`,`first_name`,`last_name`,`email`,`avatar_type`,`avatar_location`,`password`,`password_changed_at`,`active`,`confirmation_code`,`confirmed`,`timezone`,`last_login_at`,`last_login_ip`,`to_be_logged_out`,`remember_token`,`created_at`,`updated_at`,`deleted_at`,`role`,`salary`,`devices`,`phone`,`second_phone`,`id_number`,`commercial_number`,`tax_card_number`,`address`,`percentage`,`branch_id`) values (1,'ae794921-6ac8-4830-a19f-5e4d1fa941ec','Super','Admin','admin@admin.com','gravatar',NULL,'$2y$10$5xcOcqnLu.k1kTSLJFJpfett7yeHERKkVTlwiTPRPRIY4hiaJuIrq',NULL,1,'9e1c0ec6a3e792ea1a4c2191f4e38d7e',1,'America/New_York','2019-12-13 20:22:02','::1',0,'l6rJekmlKDz3ueg1pb15EW7fWxJ8EQ9IyfTj4lPclHdSpga6qgy8sTkq93k4','2019-11-09 13:53:28','2019-12-13 20:22:03',NULL,'admin',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,'2860620e-94ae-40e9-984b-82cb9c868ddf','Default','User','user@user.com','gravatar',NULL,'$2y$10$mVAiTr5Lmb9HMYGvkbe57OArfrn8id3u/aAvCg3Pyz7rTp9lkDvu2',NULL,1,'f6c56b108b52b9d6aa17ae391c6360cb',1,NULL,NULL,NULL,0,NULL,'2019-11-09 13:53:29','2019-11-09 13:53:29',NULL,'admin',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,'b8d56569-a085-48c3-b254-d46f695cf430','الفرع الرئيسيي',NULL,'main@gmail.com','gravatar',NULL,'$2y$10$1DHEZ2/dnUn.Dh5R84BafeYUDz/NfyGwEBEtaTGEhoQkeQYE/es22',NULL,1,'b217423dc6730a1f502c1b33bd405c29',1,NULL,NULL,NULL,0,NULL,'2019-11-09 13:53:30','2019-11-09 13:53:30',NULL,'branch',NULL,'vvv','01222222222',NULL,'1111111111','1111111111','1111111111','العنوان','10.00',NULL),(4,'4d87904e-3c80-41e8-ab38-00cbae160125','vdsvds','ahmed','yousef.ahmed.mohammed5030@gmail.com','gravatar',NULL,'$2y$10$Yjfvw3ECSvpqKrWHsqsLL.ALXaiEatm0JCFQy.kUUN4qgG/zVcSVa',NULL,1,NULL,0,NULL,NULL,NULL,0,NULL,'2019-11-09 14:09:41','2019-11-09 14:09:41',NULL,'technical',2000.00,'nnnn','01273975420',NULL,'29409151312017',NULL,NULL,'address example',NULL,3);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
