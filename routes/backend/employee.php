<?php

// All route names are prefixed with 'admin.auth'.
Route::group([
    'namespace' => 'Employee',
//    'middleware' => 'role:' . config('access.users.admin_role'),
    'middleware' => 'auth',

], function () {

    Route::resource('employees', 'EmployeeController');

});
