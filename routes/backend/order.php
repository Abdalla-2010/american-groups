<?php

// All route names are prefixed with 'admin.auth'.
Route::group([
    'namespace' => 'Order',
//    'middleware' => 'role:' . config('access.users.admin_role') . ','. config('access.users.secretary_role'),
    'middleware' => 'auth',
], function () {

    Route::resource('orders', 'OrderController');
    Route::get('orders/{order}/technical-assignment', 'OrderController@technicalAssignment')->name('orders.technical-assignment');
    Route::get('orders/{order}/carrier-assignment', 'OrderController@carrierAssignment')->name('orders.carrier-assignment');
    Route::get('orders/{order}/print', 'OrderController@print_order')->name('orders.print');

});
