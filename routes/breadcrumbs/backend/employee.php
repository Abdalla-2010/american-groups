<?php

Breadcrumbs::for('admin.employees.index', function ($trail) {
    $trail->push(__('labels.backend.employees.management'), route('admin.employees.index'));
});

Breadcrumbs::for('admin.employees.create', function ($trail) {
    $trail->parent('admin.employees.index');
    $trail->push(__('labels.backend.employees.create'), route('admin.employees.index'));
});


Breadcrumbs::for('admin.employees.edit', function ($trail) {
    $trail->parent('admin.employees.index');
    $trail->push(__('labels.backend.employees.edit'), route('admin.employees.index'));
});
