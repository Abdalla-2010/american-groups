<?php

Breadcrumbs::for('admin.auth.carriers.index', function ($trail) {
    $trail->push(__('labels.backend.carriers.management'), route('admin.auth.carriers.index'));
});

Breadcrumbs::for('admin.auth.carriers.create', function ($trail) {
    $trail->parent('admin.auth.carriers.index');
    $trail->push(__('labels.backend.carriers.create'), route('admin.auth.carriers.create'));
});

Breadcrumbs::for('admin.auth.carriers.edit', function ($trail) {
    $trail->parent('admin.auth.carriers.index');
    $trail->push(__('labels.backend.carriers.edit'), route('admin.auth.carriers.edit',request()->route('carrier')));
});