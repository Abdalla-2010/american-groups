<?php

Breadcrumbs::for('admin.auth.secretaries.index', function ($trail) {
    $trail->push(__('labels.backend.secretaries.management'), route('admin.auth.secretaries.index'));
});

Breadcrumbs::for('admin.auth.secretaries.create', function ($trail) {
    $trail->parent('admin.auth.secretaries.index');
    $trail->push(__('labels.backend.secretaries.create'), route('admin.auth.secretaries.create'));
});

Breadcrumbs::for('admin.auth.secretaries.edit', function ($trail) {
    $trail->parent('admin.auth.secretaries.index');
    $trail->push(__('labels.backend.secretaries.edit'), route('admin.auth.secretaries.edit',request()->route('secretary')));
});