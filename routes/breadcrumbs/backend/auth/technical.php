<?php

Breadcrumbs::for('admin.auth.technicals.index', function ($trail) {
    $trail->push(__('labels.backend.technicals.management'), route('admin.auth.technicals.index'));
});

Breadcrumbs::for('admin.auth.technicals.create', function ($trail) {
    $trail->parent('admin.auth.technicals.index');
    $trail->push(__('labels.backend.technicals.create'), route('admin.auth.technicals.create'));
});

Breadcrumbs::for('admin.auth.technicals.edit', function ($trail) {
    $trail->parent('admin.auth.technicals.index');
    $trail->push(__('labels.backend.technicals.edit'), route('admin.auth.technicals.edit',request()->route('technical')));
});