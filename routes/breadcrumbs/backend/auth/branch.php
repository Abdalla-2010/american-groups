<?php

Breadcrumbs::for('admin.auth.branches.index', function ($trail) {
    $trail->push(__('labels.backend.branches.management'), route('admin.auth.branches.index'));
});

Breadcrumbs::for('admin.auth.branches.create', function ($trail) {
    $trail->parent('admin.auth.branches.index');
    $trail->push(__('labels.backend.branches.create'), route('admin.auth.branches.create'));
});

Breadcrumbs::for('admin.auth.branches.edit', function ($trail) {
    $trail->parent('admin.auth.branches.index');
    $trail->push(__('labels.backend.branches.edit'), route('admin.auth.branches.edit',request()->route('branch')));
});