<?php

Breadcrumbs::for('admin.cities.index', function ($trail) {
    $trail->push(__('labels.backend.cities.management'), route('admin.cities.index'));
});

Breadcrumbs::for('admin.cities.create', function ($trail) {
    $trail->parent('admin.cities.index');
    $trail->push(__('labels.backend.cities.create'), route('admin.cities.index'));
});


Breadcrumbs::for('admin.cities.edit', function ($trail) {
    $trail->parent('admin.cities.index');
    $trail->push(__('labels.backend.cities.edit'), route('admin.cities.index'));
});
