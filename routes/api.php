<?php

Route::group(['prefix' => 'v1/', 'namespace' => 'Api'], function () {

    Route::post('login', 'LoginController@index');
    Route::get('cities', 'CityController@index');
    Route::post('order', 'OrderController@store');

    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('my-orders', 'OrderController@index');
        Route::post('orders/{id}/technical-response', 'OrderController@update');
    });

});
